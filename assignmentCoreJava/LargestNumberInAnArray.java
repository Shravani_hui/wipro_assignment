package assignmentCoreJava;

public class LargestNumberInAnArray {
	
	public static void main(String[] args) {
		
		int arr[] = {23,22,124,97,67};
		int temp=-1;
		
		for(int i=0;i<arr.length;i++)
		{
			for(int j=i+1;j<arr.length;j++)
			{
				if(arr[i]>arr[j])
				{
					// array sorted
					temp=arr[i];
					arr[i]=arr[j];
					arr[j]=temp;
				}
				
			}
		}
		
		System.out.println(arr[arr.length-1]);
		
	}

}
