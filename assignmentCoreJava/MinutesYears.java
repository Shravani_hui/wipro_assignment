package assignmentCoreJava;

import java.util.Scanner;

/**
 * formula of year from minutes: minutes/ 365*24*60
 * formula of days from minutes: minutes/ 24*60
 * @author Shravani
 *
 */
public class MinutesYears 
{
	public static void main(String[] args) {
		
		int year,day;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter the minutes: ");
		
		 year = (sc.nextInt())/(365*24*60);
		 day = (sc.nextInt())/(24*60);
		
		System.out.println("The year is: "+" "+year+" "+day);
	}
}
