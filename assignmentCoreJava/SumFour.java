package assignmentCoreJava;

import java.util.Scanner;

public class SumFour {
	
	public static void sum(int number)
	{
		int remainder = 0, sum=0;
		while(number>0)
		{
		remainder = number % 10;
		sum = sum + remainder;
		number= number/10;
		}
		System.out.println(sum);
		
	}
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Please enter four digit number :");
		sum(sc.nextInt());
	}

}
