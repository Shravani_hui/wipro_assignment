package automobile;

public class Honda extends Vehicle{
	private String modelName;
	private String registrationNumber;
	private String ownerName;
	private int speed;
	
	
	public Honda(String modelName, String registrationNumber, String ownerName, int speed) {
		// TODO Auto-generated constructor stub
		super();
		this.modelName = modelName;
		this.registrationNumber = registrationNumber;
		this.ownerName = ownerName;
		this.speed = speed;
	}
	
	@Override
	public void getModelName() {
		// TODO Auto-generated method stub
		System.out.println("modelName: " + modelName);
	}

	@Override
	public void getRegistrationNumber() {
		// TODO Auto-generated method stub
		System.out.println("registrationNumber: " + registrationNumber);
	}

	@Override
	public void getOwnerName() {
		// TODO Auto-generated method stub
		System.out.println("ownerName: " + ownerName);
	}

	public int getSpeed() {
		return speed;
	}
	
	public void cdplayer() {
		System.out.println("Accessing CD Player.");
	}
}
