package automobiletwowheeler;

import automobile.Vehicle;

public class Hero extends Vehicle {
	
	private String modelName;
	private String registrationNumber;
	private String ownerName;
	private int speed;
	
	public Hero(String modelName, String registrationNumber, String ownerName, int speed) {
		// TODO Auto-generated constructor stub
		super();
		this.modelName = modelName;
		this.registrationNumber = registrationNumber;
		this.ownerName = ownerName;
		this.speed = speed;
	}

	@Override
	public void getModelName() {
		// TODO Auto-generated method stub
		System.out.println("modelName: " + modelName);
	}

	@Override
	public void getRegistrationNumber() {
		// TODO Auto-generated method stub
		System.out.println("registrationNumber: " + registrationNumber);
	}

	@Override
	public void getOwnerName() {
		// TODO Auto-generated method stub
		System.out.println("ownerName: " + ownerName);
	}
	
	public int speed()
	{
		return speed;
	}
	
	public void radio()
	{
		System.out.println("Accessing the radio");
	}

}
