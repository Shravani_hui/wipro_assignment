package booktest;

public class Book {
	String isbn="", title="", author="";
	Float price,discountedprice;
	Book(String isbn,String title,String author,Float price){
		this.isbn=isbn;
		this.author=author;
		this.title=title;
		this.price=price;
	}
	
	void displaydetails(){
		System.out.println("isbn is : " + isbn);
		System.out.println("author is : " + author);
		System.out.println("title is : " + title);
		System.out.println("price is : " + price);
		System.out.println("discountedprice is : " + discountedprice);
	}
	void discountedprice(Float discount) {
		discountedprice= price - ((price*discount)/100);	
	}
	public static void main(String[] args) {
		Book b = new Book("isbn","Secret Five","Nancy Drew",100.0f);
		b.discountedprice(10f);
		b.displaydetails();
	}

}
