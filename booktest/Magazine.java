package booktest;

public class Magazine extends Book{

	String type;
	Magazine(String isbn,String title,String type,Float price){
		super(isbn,title,"",price);
		this.type=type;
	}
	public void display() {
		System.out.println("isbn is : " + isbn);
		System.out.println("type is : " + type);
		System.out.println("title is : " + title);
		System.out.println("price is : " + price);
	}
	public static void main(String[] args) {
		Magazine magzine=new Magazine("isbn_magzine","title_magazine","type_magazine",200f);
		magzine.display();
	}

}
