package booktest;

public class Novel extends Book{
	String author;
	
	Novel(String isbn,String title,String author,Float price){
		super(isbn,title,"",price);
		this.author=author;
	}
	public void display() {
		System.out.println("isbn is : " + isbn);
		System.out.println("author is : " + author);
		System.out.println("title is : " + title);
		System.out.println("price is : " + price);
	}
	public static void main(String[] args) {
		Novel novel=new Novel("isbn_novel","title_novel","author_novel",300f);
		novel.display();
	}

}
