package collectiondemo;

public class Employee {

	public static void main(String[] args) {
		
	
	
	EmployeeModel emp1 = new EmployeeModel(101, 1234, "Ram","SE","M", 25000);
	EmployeeModel emp2 = new EmployeeModel(102, 1834, "Shyam","SE","M", 35000);
	EmployeeModel emp3 = new EmployeeModel(103, 1835, "Ratika","SE","F", 55000);
	EmployeeModel emp4 = new EmployeeModel(104, 1836, "Kriti","SE","F", 65000);
	
	EmployeeDb employeedb = new EmployeeDb();
	
	
	employeedb.addEmployee(emp1);
	employeedb.addEmployee(emp2);
	employeedb.addEmployee(emp3);
	employeedb.addEmployee(emp4);

	for (EmployeeModel emp : employeedb.listAll())
		System.out.println(emp.getEmployeeInfo());
	
	
	employeedb.deleteEmployee(102);

	System.out.println();
	
	for (EmployeeModel emp : employeedb.listAll())
		System.out.println(emp.getEmployeeInfo());
	

	System.out.println(employeedb.showPaySlip(103));
	}
}
