package collectiondemo;

import java.util.ArrayList;
import java.util.Iterator;

public class EmployeeDb {

static ArrayList<EmployeeModel> employeeDb = new ArrayList<>();
	
	public boolean addEmployee(EmployeeModel e) {
		return employeeDb.add(e);
	}
	
	public boolean deleteEmployee(int empId) {
		boolean isRemoved = false;
		
		Iterator<EmployeeModel> it = employeeDb.iterator();
		
		while (it.hasNext()) {
			EmployeeModel emp = it.next();
			if (emp.getEmpId() == empId) {
				isRemoved = true;
				it.remove();
			}
		}
		
		return isRemoved;
	}
	
	public String showPaySlip(int empId) {
		String paySlip = "Invalid employee id";
		
		for (EmployeeModel e : employeeDb) {
			if (e.getEmpId() == empId) {
				paySlip = "Pay slip for employee id " + empId + " is " +
						e.getEmpSalary();
			}
		}

		return paySlip;
	}
	
	public EmployeeModel[] listAll() {
		EmployeeModel[] empArray = new EmployeeModel[employeeDb.size()];
		for (int i = 0; i < employeeDb.size(); i++)
			empArray[i] = employeeDb.get(i);
		return empArray;
	}
	

}
