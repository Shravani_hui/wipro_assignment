package collectiondemo;

public class EmployeeModel {
	
	private String emp_name,emp_designation,emp_gender;
	private int age,emp_id,emp_mobile_number;
	private float emp_salary;

	
	public EmployeeModel(int empId,int EmpMobileNumber, String empName, String empDesignation, String empGender, float empSalary) {
		emp_id = empId;
		emp_mobile_number = EmpMobileNumber;
		emp_name = empName;
		emp_designation = empDesignation;
		emp_gender = empGender;
		emp_salary = empSalary;
	}
	
	public String getEmployeeInfo() {
		return "Employee [EmpId=" + emp_id + ", EmpName=" + emp_name + ", EmpMobileNumber=" + emp_mobile_number 
				+ ", EmpGender=" + emp_gender + ", EmpSalary=" + emp_salary + ", EmpDesignation="+ emp_designation +"]";
	}
	
	
	public int getEmpId() {
		return emp_id;
	}

	public void setEmpId(int emp_name) {
		this.emp_id = emp_name;
	}

	public String getEmpName() {
		return emp_name;
	}

	public void setEmpName(String emp_name) {
		this.emp_name = emp_name;
	}

	public int getMobileNumber() {
		return emp_mobile_number;
	}

	public void setEmpEmail(int emp_mobile_number) {
		this.emp_mobile_number = emp_mobile_number;
	}

	public String getEmpGender() {
		return emp_gender;
	}

	public void setEmpGender(String emp_gender) {
		this.emp_gender = emp_gender;
	}

	public float getEmpSalary() {
		return emp_salary;
	}

	public void setEmpSalary(float emp_salary) {
		this.emp_salary = emp_salary;
	}
	
	public String getEmpDesignation() {
		return emp_designation;
	}

	public void setEmpDesignation(String emp_designation) {
		this.emp_designation = emp_designation;
	}
	
	
}
