package collectiondemo;

import java.util.HashSet;
import java.util.Iterator;

public class HashSetDemo {

	public static void main(String[] args) {
		HashSet<String> set = new HashSet<String>();
		set.add("one");
		set.add("two");
		set.add("three");
		set.add("four");
		set.add("five");
		
		Iterator<String>i= set.iterator();
		
		while(i.hasNext())
		{
			System.out.println("Value: "+ i.next());
		}
	}
}
