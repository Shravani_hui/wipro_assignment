package collectiondemo;

import java.util.HashMap;
import java.util.Scanner;

public class HashmapDemo {
	
	
	private static HashMap<String, Long> phoneBook = new HashMap<String, Long>();
	private static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {	
		
		addNameNumber();
		displayNumber();	
	}

	private static void addNameNumber() {
		// TODO Auto-generated method stub
		
		while(true)
		{
		System.out.println("Please enter a name: ");
		String name = sc.nextLine();
		if(name.equals(""))
			break;
		System.out.println("Please enter a name for number "+name+" :" );
		Long number = sc.nextLong();
		if(number==null)
			break;
		phoneBook.put(name, number);
	
		}
	}

	private static void displayNumber() {
		// TODO Auto-generated method stub
		while(true)
		{
			System.out.println("Enter the name whose number you want to look up :");
			String name = sc.nextLine();
			if(name.equals(""))
				break;
			Long number = phoneBook.get(name);
			if(number==null)
			{
				System.out.println("Number not present in phonebook");
			}
			else
			{
				System.out.println("Number is :"+number);
			}
			
		}
		
	}

}
