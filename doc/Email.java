package doc;

public class Email extends Document{
	String sender="", recepient="", title="";
	
	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getRecepient() {
		return recepient;
	}

	public void setRecepient(String recepient) {
		this.recepient = recepient;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String toString() {
		return text;
	}
	public static void main(String[] args) {
		Email email=new Email();
		email.setRecepient("recepient");
		email.setSender("sender");
		email.setTitle("title");
		email.setText("body");
		email.toString();
	}

}
