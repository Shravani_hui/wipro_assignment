package paymentpackage;

public class CreditCardPayment extends Payment{
	String Name="",ExpirationMonth="",ExpirationYear="";
	long CreditCardNumber;
	
	public CreditCardPayment(String Name, long CreditCardNumber,String ExpirationYear, String ExpirationMonth, Double payment){
		this.payment=payment;
		this.Name=Name;
		this.CreditCardNumber=CreditCardNumber;
		this.ExpirationYear=ExpirationYear;
		this.ExpirationMonth=ExpirationMonth;
	}
	public void paymentDetails(){
		System.out.println("Name is  : " + Name);
		System.out.println("CreditCardNumber is  : " + CreditCardNumber);
		System.out.println("ExpirationDate is  : " + ExpirationMonth+ "/" + ExpirationYear);
		System.out.println("Payment is  : " + payment);
	}
}
