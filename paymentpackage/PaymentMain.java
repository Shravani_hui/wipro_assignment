package paymentpackage;

public class PaymentMain {
	public static void main(String[] args) {
		CashPayment payment_one=new CashPayment(25000.00);
		CashPayment payment_two=new CashPayment(1000000.00);
		CreditCardPayment creditCardPayment_one=new CreditCardPayment("user_one",123456789097L,"2024","07",125000.00);
		CreditCardPayment creditCardPayment_two=new CreditCardPayment("user_two",123454545097L,"2026","10",425677.00);
		payment_one.paymentDetails();
		payment_two.paymentDetails();
		creditCardPayment_one.paymentDetails();
		creditCardPayment_two.paymentDetails();
	}
}