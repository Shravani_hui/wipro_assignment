package test;

import automobile.Honda;
import automobiletwowheeler.Hero;

public class TestingVehicle {

	public static void main(String[] args) {
		
		Hero hero = new Hero("Hero Maestro", "UP16G1234", "Jyoti Singh", 85);
		hero.getModelName();
		hero.getOwnerName();
		hero.getRegistrationNumber();
		hero.speed();
		hero.radio();
		
		
		System.out.println();
		
		Honda honda = new Honda("Honda Civic", "UP18G7777", "Monika Sharma", 120);
		honda.getModelName();
		honda.getOwnerName();
		honda.getRegistrationNumber();
		honda.getSpeed();
		honda.cdplayer();

	}
}
