package testing;

import java.util.Scanner;

public class AcceptInteger {
	
	public static void main(String[] args) {
		int num;
		int []arr = new int[args.length];
		double avg=0;
	
		for(int i=0;i<args.length;i++) {
			num=Integer.parseInt(args[i]);
			arr[i]=num;
			avg+=num;
			System.out.println("Number "+i+" is : "+num);
		}
		try {
			if(arr.length!=5) {
				throw new ArrayIndexOutOfBoundsException();
			}
			else {
				avg=avg/arr.length;
				System.out.println("Average is : " + avg);
			}		
		}
		catch(ArrayIndexOutOfBoundsException e) {
			System.out.println(e + ". Please enter 5 integers");
		}
					
	}


}
