package testing;

import java.io.InputStream;
import java.util.Scanner;

@SuppressWarnings("serial")
class InvalidAgeException extends Exception{
	@Override
	public String toString() {
		return ("Age must be >=18 and <60");
	}
}

public class AgeCheck {

	public static void main(String[] args) /* throws InvalidAgeException */ {
		Scanner sc = new Scanner(System.in);
		System.out.println("Please enter name and age : ");
		String name = sc.nextLine();
		int age = sc.nextInt();
//		int age = Integer.parseInt(args[1]);
		if(age>=18 && age<60)
			System.out.println(name+" has proper Age");
		else
			try {
				throw new InvalidAgeException();
			}
			catch(InvalidAgeException ex) {
				ex.printStackTrace();
			}
	}

	}


