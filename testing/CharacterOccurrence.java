package testing;

import java.util.Scanner;

public class CharacterOccurrence {

	public static void main(String[] args) {
		System.out.println("Enter String : ");
		Scanner scan=new Scanner(System.in);
		String str=scan.nextLine();
		System.out.println("Enter Character to be searched : ");
		String c=scan.next();
		scan.close();
		int count = str.length() - str.replace(c, "").length();
		System.out.println("Occurrences of "+c+ " in String "+str + " is " + count);
	}
}
