package thread;

import java.util.Random;

public class Fac implements Runnable{
	
	int number;
	Fac(int number)
	{
		this.number=number;
	}

	public int factorial(int n)
	{
		if(n==0 || n==1)
		{
			return 1;
		}
		else
		{
		return n*factorial(n-1);
		}
	}

	public static void main(String[] args) {
		
		/**
		 * Random number generated in main thread and factorial is generated in worker thread
		 */
			Random random= new Random();
			int number = random.nextInt(10);
			
			System.out.println(number);
			
			
			Fac fac= new Fac(number);
			Thread t1= new Thread(fac);
			t1.start();
			
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		System.out.println(factorial(number));

	}
}
