package thread;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class Interval extends TimerTask {

	public static void main(String[] args) {
	
		TimerTask timertask = new Interval();
		Timer t = new Timer();
		t.scheduleAtFixedRate(timertask, 0, 2000);
	
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        System.out.println( "Current time is: " + sdf.format(cal.getTime()));
	}
}


